(require 'beeminder)

(defun tomato-notify (summary &optional body)
  "Show the user a notification using notify-send."
  (call-process "notify-send" nil nil nil
		"-a" "Tomato" (format "%s" summary) (format "%s" (or body ""))))

(defvar tomato-duration 25
  "Tomato duration in minutes.")

(defvar tomato--timer nil
  "A timer for showing tomato notifications.")

(defcustom tomato-sound-file nil
  "The sound to play when a tomato ends.")

(defcustom tomato-beeminder-goal "tomatoes"
  "The name of the Beeminder goal for tomatoes.")

(defun tomato-clock-in ()
  "Start the tomato timer."
  (when (timerp tomato--timer)
    (cancel-timer tomato--timer))
  (setq tomato--timer
	(run-with-timer (* tomato-duration 60)
			(* tomato-duration 60)
			(lambda ()
			  (start-process "tomato-end-sound" nil "mplayer" tomato-sound-file)
			  (tomato-notify "A tomato has just ended.")))))

(defun tomato-clock-out ()
  "Check if the clock qualifies as a tomato.
This means at least `tomato-duration' minutes."
  (when (timerp tomato--timer)
    (cancel-timer tomato--timer))
  (setq tomato--timer nil)
  (when (not (string-match " LINE REMOVED$" (or (current-message) ""))) ; hack
    (let ((minutes (org-duration-to-minutes (org-element-property :duration
								  (org-element-at-point))))
	  (headline (or
		     (org-entry-get (point) "tomato-title" t)
		     (substring-no-properties (org-get-heading t t t t)))))
      (when (>= minutes tomato-duration)
	(beeminder-submit-datapoint
	 tomato-beeminder-goal
	 (floor minutes tomato-duration)
	 headline)))))

(defun tomato-clock-cancel ()
  "Cancel the tomato timer."
  (when (timerp tomato--timer)
    (cancel-timer tomato--timer)))

(define-minor-mode tomato-mode
  "A global minor mode tracking number of pomodoros with beeminder.el."
  :init-value nil
  :global t
  :lighter " ó"
  (if tomato-mode
      (progn (add-hook 'org-clock-in-hook #'tomato-clock-in)
	     (add-hook 'org-clock-out-hook #'tomato-clock-out)
	     (add-hook 'org-clock-cancel-hook #'tomato-clock-cancel))
    (remove-hook 'org-clock-in-hook #'tomato-clock-in)
    (remove-hook 'org-clock-out-hook #'tomato-clock-out)
    (remove-hook 'org-clock-cancel-hook #'tomato-clock-cancel)))
